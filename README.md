# bstokml

This project is indented to take BaseStation formatted log files and convert them to KML paths for importing to GIS tools like Google Earth or ArcGIS Earth. 
The idea is that you can capture ADS-B out messages using an RTL-SDR dongle, and dump1090 software. Then convert the messages to visualized flight paths. 
This is particularly useful if you live near an airport and wish to visualize the flight paths of actual aircraft in the area. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This software is written in Python 3. It assumes you are using BaseStation formatted CSV files. The easiest way to create these is with a version of dump1090. 
You can filter the messages out easily with grep and then cat them to a file. Make sure you set the correct lat and long for the location of your RTL-SDR Antenna. 
The current version of dump1090 I recommend is [dump1090-fa](https://github.com/adsbxchange/dump1090-fa) as it is the most recently maintained fork. 

```
dump1090 --net --lat [RX_LAT] --lon [RX_LON] --fix
```

Once you have the dump1090 software running and receiving messages, you can start logging them to a file for processing. An easy way to do this is to use telnet to connect to the output port, and grep to filter the message types.
The machine that is logging the data does not have to be the same machine that is running dump1090. This means you can possibly position your RTL-SDR dongle some where better for reception, 
and connected to a low power device like a Raspberry Pi, then log it remotely. 

```
telnet [HOST_ADDRESS] 30003 | grep "MSG,3," >> pos_log.csv
```

>Note: If you want to enable the spatial support (select only trips within a bounding set of polygons) you will need to also install [libspatialite](https://www.gaia-gis.it/fossil/libspatialite/index). While optional, it is reccomended. 

### Installing

Once you have dump1090 installed and working, and a log file generated, you are ready to generate flight paths or contribute to the project. 

We need to first clone the project to get a copy of the code. 

```
git clone git@gitlab.com:btreecat/bstokml.git
```

If you do not have SSH keys configured with Gitlab, you can use HTTPS instead

```
git clone https://gitlab.com/btreecat/bstokml.git
```

Next move into the cloned directory

```
cd bstokml/
```

Now we need to set up your environment. First step is to create a virtual environment for dependency isolation. The environment name is the name of the directory the python virtual environment will be stored. 

```
python -m venv [ENVIRONMENT_NAME]
```

Next activate The virtual environment. There are multiple activation scripts, choose the correct one for your current shell. 

```
source [ENVIORNMENT_NAME]/bin/activate.[SHELL]
```

With the virtual environment activated, use pip to install the required dependencies. 

>NOTE: pip itself may complain about not being up to date, feel free to ignore this message or update it to the latest version

```
pip install -r requirements.txt
```

You also want a copy of the [FAA aircraft registration data.](https://www.faa.gov/licenses_certificates/aircraft_certification/aircraft_registry/releasable_aircraft_download/)

On this page, you will want to download the zip file of the "Aircraft Registration Database" and extract it. 

You want the file MASTER.txt, which is just a CSV file of the registration data. 

Now your environment is ready to process the input data from dump1090 logs. 

## Running bstokml

The application has only a CLI with several flags. Many defaults may not be appropriate for your area or the elevations you are interested in visualizing. 
Be sure to check the defaults and change whatever values are appropriate for the dataset you are working with. 

### Show help menu

You can have bstokml print all it's options to STDOUT with the --help flag. 

```
python bstokml.py --help
```

This will list the most up to date information on the various command line arguments. 

### Options

Currently there are many options that can be used based on your exact workflow or the data you are interested in. 

>Note: The important options for most people will be altmax, altland, and floor because they are all relative to local values.


| Option | Parameters | Description | Default |
|---|---|---|---|
|--msg|MSG [MSG ...]|A CSV file with message type 3 entries for processing.|None|
|--merge||If set, instead of dropping the table and starting fresh, attempt to merge the CSV into an existing SQLite DB. Requires --csv to also be enabled.|False|
|--db|DB|Filename of SQLite DB.|data.db|
|--kml|KML|KML file to write the flight paths to.|flight_paths.kml|
|--altmax|ALTMAX|Max altitude of flight path processing. Filters out high altitude flyovers. Feet MSL.|3000|
|--altland|ALTLAND|Minimum Altitude needed to trigger a landing detection. Filters out low altitude flyovers. Feet MSL|2300|
|--gap|GAP|How long of a gap between messages from a unique aircraft on the same date should be allowed before breaking the data into multiple trips. Seconds|200|
|--reduce|REDUCE|Minimum number of trip messages before reduced sampling is used.|10000|
|--freq|FREQ|Frequency of message sampling once reduction is triggered.|10|
|--floor|FLOOR|Local ground floor to help when the interpolation generates below ground values. Feet MSL|2000|
|--aircraft|AIRCRAFT [AIRCRAFT ...]|Only output paths for the specified aircraft. Use either ICAO hex or FAA N-Number. Can be combined with --date.|None|
|--date|DATE [DATE ...]|Only output paths for the specified date YYYY-MM-DD. Range can be specified as two dates. Can be combined with --aircraft.|None|
|--faa|FAA|File for the FAA CVS to look up Tail numbers. Will try to update existing table. [FAA aircraft registration data.](https://www.faa.gov/licenses_certificates/aircraft_certification/aircraft_registry/releasable_aircraft_download/)|None|
|--poly|POLY|A GeoJSON or WKT CSV file of the MULTIPOLYGON geometries. Only trips that intersect this polygon will be selected. Requires [SpatiaLite](https://www.gaia-gis.it/fossil/libspatialite/index)|None|

### Generateing output

You will want to load at least one CSV into the database as well as at least one copy of the FAA registration data. 
Once the FAA registration info has been processed, you do not need to reprocess it unless you need to update any registration info or if you delete the database file. 

The message log file also only needs to be loaded once unless you want to add more messages to the database. 

To import both initial messages and initial FAA registration you can include both options. 

```
python bstokml.py --csv [CSV_FILE] --faa [MASTER.txt] 
```

On subsequent runs, -faa and --csv can be left off to improve speed. 

If you wish to merge a new CSV file with existing messages in the database, use the --merge option with the --csv option. 

```
python bstokml.py --csv [CSV_FILE] --merge
```

You may use more than one CSV file as input and have all messages combined into the database. 

```
python bstokml.py --csv [CSV_FILE_0] [CSV_FILE_1] ... [CSV_FILE_N]
```

To filter out a specific date --date flag and pass a date in the format of YYY-MM-DD. 

```
python bstokml.py --date [DATE]
```

For a range of dates, pass in two dates

```
python bstokml.py --date [DATE_0] [DATE_1]
```

To search by aircraft, there is the --aircraft option. This can take either an FAA N/Tail Number or an ICAO hex ID and can be as many aircraft ID as you wish, mixing and matching either format. Can also be combined with --date. 

```
python bstokml.py --aircraft [N_NUMBER_0] [N_NUMBER_1] ... [N_NUMBER_N] --date [DATE]
```

If you have [SpatiaLite](https://www.gaia-gis.it/fossil/libspatialite/index) installed, you can create one or more polygons to use as an intersecting region for flight paths. Only flight paths that intersect the provided polygons will be shown in the final KML output. By default, [GeoJSON](https://en.wikipedia.org/wiki/GeoJSON) format is assumed. 

```
python bstokml.py --poly poly.geojson
```

Alternatively you can use a CSV formatted [Well Known Text (WKT)](https://en.wikipedia.org/wiki/Well-known_text_representation_of_geometry) 

```
python bstokml.py --poly poly.csv
```

You can create polygons and export the files with tools like ArcGis or [QGIS](https://www.qgis.org/en/site/)

![alt text](vtti_polygon.png "Example of a polygon that can be used to filter flight paths. Created in QGIS.")

## Output

The resulting KML file will have randomly assigned colors to each flight path. Also additional meta data is added to each flight path, as well as the KML file itself. 
You can import output kml file directly into Google Earth for viewing. If you re-run the application with the same input, you should get the same out put flight paths, with new random colors applied. 
Sometimes the colors generated are too similar, and re-generating may produce a more visually pleasing representation.  

![alt text](bburg_flight_paths_2019-09-26_2019-09-28.jpg "Overhead view of Blacksburg VA flight paths from 2019-09-26 to 2019-09-28")


### KML Metadata

The KML file includes information about some of the flags used to generate the data including:

* Date Range
* Total flight paths 
* Total unique aircraft
* Altitude Max
* Altitude Landing

![alt text](kml_meta_data.jpg "Metadata for final kml file that is produced")


### Placemark Metatdata

Each flight path is a [KML Placemark](https://developers.google.com/kml/documentation/kml_tut#placemarks). 

These placemarks have additional meta data that is unique to the flight path including:

* ICAO Hex
* Date
* Trip ID (0 index ID of trips for an aircraft on the same date)
* N-Number
* Aircraft Type
* Aircraft Engine
* Aircraft Year
* Registered Owner Name
* Registered City
* Registered State
* First Message Received Time
* Last Message Received Time
* Minimum Altitude
* Max Altitude
* Type of Trip

![alt text](flight_meta_data.jpg "Metadata for flight path of N144VT on 2019-09-27")


## Technologies Leveraged

* [RTL-SDR](https://www.rtl-sdr.com/buy-rtl-sdr-dvb-t-dongles/) - Required hardware
* [dump1090-fa](https://github.com/adsbxchange/dump1090-fa) - Branch of dump1090 used
* [Python](https://www.python.org/) - Language
* [SQLite](https://www.sqlite.org/index.html) - Database
* [SpatiaLite](https://www.gaia-gis.it/fossil/libspatialite/index) - GIS functions
* [SciPy](https://www.scipy.org/) - Interpolation
* [QGIS](https://www.qgis.org/en/site/) - Polygon Creation and SpatiaLite validation
* [Google Earth](https://www.google.com/earth/) - KML Viewing


## Contributing

Open to PR, suggestions, fixes etc. Thank you. 


## Authors

* **Stephen Tanner** - *Initial work* - [btreecat](https://gitlab.com/btreecat)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Thanks to woodair.net/Bones Aviation Page for the [BaseStation format](http://woodair.net/SBS/Article/Barebones42_Socket_Data.htm)
* Thanks to the [Virginia Tech Transportation Institute](https://www.vtti.vt.edu/) For funding the development
* Thanks to PurpleBooth for the [README.md template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
* Thanks to [PlaneFinder](https://planefinder.net/) for the real time tracking of aircraft
* Thanks to [ADS-B Exchange](https://adsbexchange.com/) for maintaining dump1090-fa

