import argparse

METER = 0.3048
ALT_RES = 125
DATE_FMT = "%Y-%m-%d %H:%M:%S.%f"
INTERPOLATION_TYPE = "quadratic"
CRS = '"crs":{"type":"name","properties":{"name":"EPSG:4326"}}'
SRID = 4326

arg_parser = argparse.ArgumentParser(description='''A CLI tool to convert from BaseStation CSV message type 3 to a KML 
                                                    file for display on Google Earth. The best way to generate the data 
                                                    is to use dump1090 with the --net flag. Then telnet to the port 
                                                    30003, grep for 'MSG,3,' and > to a csv file.''',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

arg_parser.add_argument('--msg', help="A CSV file with message type 3 entries for processing.", nargs='+')
arg_parser.add_argument('--merge', help="If set, instead of dropping the table and starting fresh, attempt to merge "
                                        "the CSV into an existing SQLite DB. Requires --msg to also be enabled.",
                        action='store_true')
arg_parser.add_argument('--db', help="Filename of SQLite DB.", default="data.db")
arg_parser.add_argument('--kml', help="KML file to write the flight paths to.", default="flight_paths.kml")
arg_parser.add_argument('--altmax', type=int, help="Max altitude of flight path processing. Filters out high altitude "
                                                   "flyovers. Feet MSL.", default=3000)
arg_parser.add_argument('--altland', type=int, help="Minimum Altitude needed to trigger a landing detection. "
                                                    "Filters out low altitude flyovers. Feet MSL", default=2300)
arg_parser.add_argument('--gap', type=int, help="How long of a gap between messages from a unique aircraft on the "
                                                "same date should be allowed before breaking the data into multiple "
                                                "trips. Seconds", default=200)
arg_parser.add_argument('--reduce', type=int, help="Minimum number of trip messages before reduced sampling is used.",
                        default=10000)
arg_parser.add_argument('--freq', type=int, help="Frequency of message sampling once reduction is triggered.",
                        default=10)
arg_parser.add_argument('--floor', type=int, help="Local ground floor to help when the interpolation generates "
                                                  "below ground values. Feet MSL", default=2000)
arg_parser.add_argument('--aircraft', help="Only output paths for the specified aircraft. Use either ICAO hex or "
                                           "FAA N-Number. Can be combined with --date.", nargs='+')
arg_parser.add_argument('--date', help="Only output paths for the specified date YYYY-MM-DD. Range can be specified "
                                       "as two dates. Can be combined with --aircraft.", nargs="+")
arg_parser.add_argument('--faa', help="File for the FAA CVS to look up Tail numbers. Will try to "
                                      "update existing table.")
arg_parser.add_argument('--poly', help="A GeoJSON or WKT CSV file of the MULTIPOLYGON geometries. "
                                       "Only trips that intersect this polygon will be selected. Requires SpatiaLite.")

aircraft_types = {'1': "Glider",
                  '2': "Balloon",
                  '3': 'Blimp/Dirigible',
                  '4': 'Fixed wing single engine',
                  '5': 'Fixed wing multi engine',
                  '6': 'Rotorcraft',
                  '7': 'Weight-shift-control',
                  '8': 'Powered Parachute',
                  '9': 'Gyroplane',
                  'H': 'Hybrid Lift',
                  'O': 'Other'}

aircraft_engines = {'0': 'None',
                    '1': 'Reciprocating',
                    '2': 'Turbo-prop',
                    '3': 'Turbo-shaft',
                    '4': 'Turbo-jet',
                    '5': 'Turbo-fan',
                    '6': 'Ramjet',
                    '7': '2 Cycle',
                    '8': '4 Cycle',
                    '9': 'Unknown',
                    '10': 'Electric',
                    '11': 'Rotary'}
