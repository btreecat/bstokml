import csv
import datetime
import random
import sqlite3
import numpy as np
from scipy.interpolate import interp1d
import config as cfg
import queries
import sys
import json


def random_rgb():
    return random.randint(0, 255)


def calc_trip_type(alt_start, alt_end):
    if abs(alt_start - alt_end) <= cfg.ALT_RES:
        trip_type = "Round Trip"
    elif (alt_start - alt_end) > 0:
        trip_type = "Landing"
    else:
        trip_type = "Take-off"
    return trip_type


def init_trips(db_cursor):
    db_cursor.execute(queries.drop_trips)
    db_cursor.execute(queries.create_trips)
    db_cursor.execute(queries.add_geo_trips, [cfg.SRID])


def load_spatial(db_cursor, db_conn):
    try:
        db_conn.enable_load_extension(True)
        db_cursor.execute(queries.load_spatial)
        db_conn.enable_load_extension(False)
        print("mod_spatialite has been loaded")
    except sqlite3.OperationalError as err:
        print(err)
        print("Unable to load libspatialite module. Please check your install of libspatialite.")
        sys.exit(1)


def init_spatial(db_cursor):
    try:
        db_cursor.execute(queries.init_check)
        print("Spatial Metadata Tables Exist")
    except sqlite3.OperationalError as err:
        if 'no such table: geometry_columns' in err.args[0]:
            print("Initializing Spatial Metadata Tables")
            db_cursor.execute(queries.init_spatial)


def parse_registration(faa_csv):
    faa_reader = csv.reader(faa_csv, delimiter=',')
    header = faa_reader.__next__()
    clean_regs = [(faa[0].strip(), int(faa[4]) if faa[4].isdigit() else None, faa[6].strip(), faa[9].strip(),
                   faa[10].strip(), cfg.aircraft_types[faa[18].strip()], cfg.aircraft_engines[faa[19].strip()],
                   faa[33].strip())
                  for faa in faa_reader]
    return clean_regs


def parse_msgs(msg_csv):
    msg_reader = csv.reader(msg_csv, delimiter=',')
    clean_msgs = []
    for msg in msg_reader:
        if len(msg) == 22:
            if msg[14].replace('.', '').replace('-', '').isdigit() and msg[15].replace('.', '').replace('-',
                                                                                                        '').isdigit():
                clean_msgs.append((msg[4], f"{msg[6].replace('/', '-')} {msg[7]}", msg[11], msg[14], msg[15]))
    return clean_msgs


def poly_init(db_cursor):
    # Need to check first if these already exists\
    db_cursor.execute(queries.drop_polygons)
    db_cursor.execute(queries.create_polygons)
    db_cursor.execute(queries.add_geo_poly, [cfg.SRID])


def create_tables(db_cursor, args):
    print("Creating DB Tables")
    if not args.merge and args.msg:
        db_cursor.execute(queries.drop_messages)
    if args.poly:
        poly_init(db_cursor)
        init_trips(db_cursor)
        # calling a second time because initial call does not seem to fully setup all metadata for spatialite
        init_trips(db_cursor)
    db_cursor.execute(queries.create_messages)
    db_cursor.execute(queries.create_registrations)


def sample_trip(trip, args):
    total_samples = int(trip[0][-1] - trip[0][0])
    if total_samples >= args.reduce:
        total_samples = total_samples / args.freq

    sample_range = np.linspace(trip[0][0], trip[0][-1], total_samples)
    npa_times = np.array(trip[0])
    npa_points = np.transpose(np.array(trip[1]))
    trip_function = interp1d(npa_times, npa_points, kind=cfg.INTERPOLATION_TYPE)
    trip_samples = trip_function(sample_range)
    return trip_samples


def points_to_wkt_linestring(points):
    point_str = ''
    for idx in range(0, len(points[0])):
        point_str += f'{points[0, idx]:.5f} {points[1, idx]:.5f} {int(points[2, idx])},'

    wkt = f"LINESTRING Z({point_str[:-1]})"
    return wkt


def insert_poly(db_cursor, db_conn, args):
    try:
        # Frist just assume it's a geojson format file
        with open(args.poly) as poly_json:
            features = json.load(poly_json)['features']
            for idx, feature in enumerate(features):
                if feature['geometry']['coordinates']:
                    geometry = str(feature['geometry']).replace("'", '"')
                    db_cursor.execute(queries.insert_poly_json, [f'area_{idx}', geometry, cfg.SRID])

    except json.decoder.JSONDecodeError as err:
        # If that failed, then go ahead and assume it's a WKT CSV
        with open(args.poly) as poly_csv:
            poly_reader = csv.reader(poly_csv, delimiter=',')
            header = poly_reader.__next__()
            for idx, poly in enumerate(poly_reader):
                if "EMPTY" not in poly[0]:
                    db_cursor.execute(queries.insert_poly, [f'area_{idx}', poly[0], cfg.SRID])
    db_conn.commit()


def wkt_array(wkt):
    coords = wkt[13:-1].split(',')
    return [coord.lstrip().split(' ') for coord in coords]


def make_placemark(trip, aircraft_registration, placemark_template, args):
    coords = ""
    if args.poly:
        points = wkt_array(trip[5])
    else:
        points = trip[1]
    alt_max = int(points[0][2])
    alt_min = alt_max
    trip_type = calc_trip_type(int(points[0][2]), int(points[-1][2]))
    for point in points:
        altitude = int(point[2])
        if altitude > alt_max:
            alt_max = altitude
        if altitude < alt_min:
            alt_min = altitude if altitude >= args.floor else args.floor
        coords += f"{float(point[0]):.5f},{float(point[1]):.5f},{int(altitude * cfg.METER) if altitude >= args.floor else int(args.floor * cfg.METER)}\n"

    hex_color = 'FF%02X%02X%02X' % (random_rgb(), random_rgb(), random_rgb())

    if args.poly:
        timestamp_first = datetime.datetime.fromtimestamp(float(trip[2]))
        timestamp_last = datetime.datetime.fromtimestamp(float(trip[3]))
        aircraft_id = trip[0]
        trip_id = trip[1]
    else:
        timestamp_first = datetime.datetime.fromtimestamp(float(trip[0][1]))
        timestamp_last = datetime.datetime.fromtimestamp(float(trip[0][-1]))
        aircraft_id = trip[2]
        trip_id = trip[3]

    fmt_placemark = placemark_template.format(aircraft_id=aircraft_id,
                                              trip_date=f'{timestamp_first.year}-{timestamp_first.month}-{timestamp_first.day}',
                                              coordinates=coords,
                                              hex_color=hex_color,
                                              trip_id=trip_id,
                                              tail_number=aircraft_registration[0],
                                              aircraft_year=aircraft_registration[1] if aircraft_registration[
                                                  1] else "",
                                              registrant_name=aircraft_registration[2].replace("&", "&amp;"),
                                              registrant_city=aircraft_registration[3],
                                              registrant_state=aircraft_registration[4],
                                              aircraft_type=aircraft_registration[5],
                                              aircraft_engine=aircraft_registration[6],
                                              message_first=f'{timestamp_first.hour}:{timestamp_first.minute}:{timestamp_first.second}',
                                              message_last=f'{timestamp_last.hour}:{timestamp_last.minute}:{timestamp_last.second}',
                                              alt_min=alt_min,
                                              alt_max=alt_max,
                                              trip_type=trip_type)

    return fmt_placemark


def make_header(stats, header_template, args):
    # Include some basic reporting info in the header of the KML file
    fmt_header = header_template.format(alt_max=args.altmax,
                                        alt_land=args.altland,
                                        gap=args.gap,
                                        date_start=stats['date_start'],
                                        date_end=stats['date_end'],
                                        unique_aircrafts=stats['unique'],
                                        total_trips=stats['total'])

    return fmt_header


def main():
    args = cfg.arg_parser.parse_args()
    # setup DB
    db_conn = sqlite3.connect(args.db)
    db_cursor = db_conn.cursor()

    if args.poly:
        load_spatial(db_cursor, db_conn)
        init_spatial(db_cursor)

    create_tables(db_cursor, args)

    if args.faa:
        with open(args.faa) as faa_csv:
            print(f'Reading FAA Registrations: {args.faa}')
            clean_regs = parse_registration(faa_csv)

        db_cursor.executemany(queries.replace_registrations, clean_regs)
        db_conn.commit()

    # Insert messages from CSV into SQLite
    msg_counts = []
    if args.msg:
        for msgs in args.msg:
            print(f'Reading Messages CSV: {msgs}')
            with open(msgs, newline='') as msg_csv:
                clean_msgs = parse_msgs(msg_csv)

            db_cursor.executemany(queries.insert_messages, clean_msgs)

            msg_counts.append(len(clean_msgs))
        db_conn.commit()
        print(f"CSVs Read: {len(args.msg)}")
        print(f"Total Messages Read: {sum(msg_counts)}")
        print("DB Populated")

    # Fetch unique aircraft ID per date
    sel_aircraft = []

    if args.aircraft:
        for aircraft in args.aircraft:
            if aircraft[0].lower() == 'n':
                sel_aircraft.append(db_cursor.execute(queries.select_icao, [aircraft[1:]]).fetchone()[0])
            else:
                sel_aircraft.append(aircraft)

    params = []
    if sel_aircraft and args.date:
        fmt_params = ','.join('?' * len(sel_aircraft))
        qry = queries.select_flights_aircraft_date.format(aircraft_list=fmt_params)
        params.append(args.altland)
        params += sel_aircraft
        params.append(args.date[0])
        if len(args.date) > 1:
            params.append(args.date[1])
        else:
            params.append(args.date[0])
    elif sel_aircraft:
        fmt_params = ','.join('?' * len(sel_aircraft))
        qry = queries.select_flights_aircraft.format(aircraft_list=fmt_params)
        params.append(args.altland)
        params += sel_aircraft
    elif args.date:
        qry = queries.select_flights_date
        params.append(args.altland)
        params.append(args.date[0])
        if len(args.date) > 1:
            params.append(args.date[1])
        else:
            params.append(args.date[0])
    else:
        params = [args.altland]
        qry = queries.select_flights

    db_cursor.execute(qry, params)
    flight_days = db_cursor.fetchall()

    # kml template setup
    with open('kml_head') as kml_head:
        template_header = kml_head.read()

    with open('kml_tail') as kml_tail:
        template_footer = kml_tail.read()

    with open('kml_body') as kml_body:
        template_placemark = kml_body.read()

    flight_trips = []
    for flight in flight_days:
        aircraft_id = flight[0]
        message_date = flight[1]
        db_cursor.execute(queries.select_messages, [aircraft_id, args.floor, args.altmax, message_date])

        # Fetch all the path points for a given hex airaft_id and a date
        path_points = db_cursor.fetchall()
        times = []
        positions = []
        daily_trips = []
        # We have to break the individual trips down due to the same plane taking off/landing multiple times in one day
        for index in range(0, len(path_points)):
            if index != 0:
                time_prior = datetime.datetime.strptime(f"{path_points[index - 1][3]}000", cfg.DATE_FMT)
                time_current = datetime.datetime.strptime(f"{path_points[index][3]}000", cfg.DATE_FMT)
                time_diff = (time_current - time_prior).seconds
                if time_diff >= args.gap:
                    # Message gap has detected a seperate trip on the same date
                    if len(times) > 3:
                        # We need at least 4 data points for the interpolation to work with out throwing an error due to it being set on cubic
                        daily_trips.append([times, positions])
                    # clear the times/positions in prep for the next trip
                    times = []
                    positions = []

            times.append(datetime.datetime.strptime(f"{path_points[index][3]}000", cfg.DATE_FMT).timestamp())
            positions.append(path_points[index][0:3])
        # After the loop, make sure any final trips are stored
        if len(times) > 3:
            daily_trips.append([times, positions])

        print(f"Plane: {aircraft_id}\tDate: {message_date}\tTrips: {len(daily_trips)}")

        for trip_id, trip in enumerate(daily_trips):
            message_first = trip[0][0]
            # message_first = f"{message_first.hour}:{message_first.minute}:{message_first.second}"
            message_last = trip[0][-1]
            # message_last = f"{message_last.hour}:{message_last.minute}:{message_last.second}"

            trip_samples = sample_trip(trip, args)

            linestring = points_to_wkt_linestring(trip_samples)
            if args.poly:
                params = [aircraft_id, trip_id, message_first, message_last, linestring, cfg.SRID]
                # db_cursor.execute(queries.insert_trip, params)
                flight_trips.append(params)
            else:
                #the trip object does not have __dict_ so I can not append or add attributes
                trip = [trip[0], trip[1], aircraft_id, trip_id]
                try:
                    aircraft_registration = db_cursor.execute(queries.select_registration, [aircraft_id]).fetchone()
                except sqlite3.InterfaceError as err:
                    aircraft_registration = ((''), (''), (''), (''), (''), (''), (''), (''))
                finally:
                    if not aircraft_registration:
                        aircraft_registration = ((''), (''), (''), (''), (''), (''), (''), (''))
                flight_trips.append(make_placemark(trip, aircraft_registration, template_placemark, args))
            print(f"Trip ID: {trip_id}\tSamples: {len(trip_samples[0])}")

    # print(f"Total Trips Processed: {len(flight_trips)}")
    date_set = sorted({s[1] for s in flight_days})
    aircraft_set = {s[0] for s in flight_days}

    if args.poly:
        insert_poly(db_cursor, db_conn, args)
        db_cursor.executemany(queries.insert_trip_geom, flight_trips)
        db_conn.commit()
        trips = db_cursor.execute(queries.intersecting_trips).fetchall()
        stats = {'unique': len(aircraft_set), 'total': len(trips), 'date_start': date_set[0], 'date_end': date_set[-1]}

    else:
        if date_set:
            stats = {'unique': len(aircraft_set), 'total': len(flight_trips), 'date_start': date_set[0],
                     'date_end': date_set[-1]}
        else:
            stats = {'unique': len(aircraft_set), 'total': len(flight_trips), 'date_start': "None",
                     'date_end': "None"}

    # write our KML file template_header
    output_kml = open(args.kml, 'w')
    output_kml.write(make_header(stats, template_header, args))
    # Write out the individual flight paths
    if args.poly:
        for trip in trips:
            aircraft_registration = db_cursor.execute(queries.select_registration, [trip[0]]).fetchone()
            output_kml.write(make_placemark(trip, aircraft_registration, template_placemark, args))
    else:
        for pm in flight_trips:
            output_kml.write(pm)

    output_kml.write(template_footer)
    output_kml.close()
    print(f"KML file written: {args.kml} ")


if __name__ == "__main__":
    main()
