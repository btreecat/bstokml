init_spatial = '''SELECT InitSpatialMetaData(1)'''

init_check = '''SELECT * FROM geometry_columns'''

load_spatial = '''SELECT load_extension('mod_spatialite')'''

add_geo_messages = '''SELECT AddGeometryColumn('messages', 'geom', ?, 'POINT')'''

add_geo_trips = '''SELECT AddGeometryColumn('trips', 'geom', ?, 'LINESTRING')'''

add_geo_poly = '''SELECT AddGeometryColumn('polygons', 'geom', ?, 'MULTIPOLYGON')'''

drop_messages = '''DROP TABLE IF EXISTS messages'''

drop_trips = '''DROP TABLE IF EXISTS trips'''

drop_polygons = '''DROP TABLE IF EXISTS polygons'''

create_polygons = '''CREATE TABLE IF NOT EXISTS polygons (name TEXT UNIQUE)'''

create_messages = '''CREATE TABLE IF NOT EXISTS messages
                        (aircraft_id TEXT, 
                        message_timestamp TEXT,  
                        aircraft_altitude INTEGER, 
                        aircraft_lat REAL, 
                        aircraft_lon REAL,
                        UNIQUE (aircraft_id, message_timestamp))'''

create_registrations = '''CREATE TABLE IF NOT EXISTS registrations
                        (tail_number TEXT UNIQUE,
                        aircraft_year INTEGER nullable, 
                        registrant_name TEXT,
                        registrant_city TEXT,
                        registrant_state TEXT,
                        aircraft_type TEXT,
                        aircraft_engine TEXT,
                        aircraft_id TEXT UNIQUE)'''

create_trips = '''CREATE TABLE IF NOT EXISTS trips
                    (aircraft_id TEXT,
                    trip_id INTEGER,
                    trip_time_start TEXT,
                    trip_time_end TEXT)'''

replace_registrations = '''REPLACE INTO registrations values(?,?,?,?,?,?,?,?)'''

insert_trip_geom = '''INSERT INTO trips VALUES(?,?,?,?,GeomFromText(?,?))'''

insert_trip = '''INSERT INTO trips VALUES(?,?,?,?,)'''

insert_poly = '''INSERT INTO polygons VALUES(?,GeomFromText(?,?))'''

insert_poly_json = '''INSERT INTO polygons VALUES(?,SetSRID(GeomFromGeoJSON(?),?))'''

insert_geom_messages = '''INSERT INTO messages VALUES(?,?,?,?,?,MakePointZ(?,?,?,?))
                        ON CONFLICT (aircraft_id, message_timestamp)
                        DO NOTHING'''

insert_messages = '''INSERT INTO messages VALUES(?,?,?,?,?)
                        ON CONFLICT (aircraft_id, message_timestamp)
                        DO NOTHING'''

select_flights = '''SELECT DISTINCT aircraft_id,Date(message_timestamp) 
                    FROM messages 
                    WHERE aircraft_altitude <= ?
                    ORDER BY aircraft_id'''

select_flights_aircraft = '''SELECT DISTINCT aircraft_id,Date(message_timestamp) 
                            FROM messages 
                            WHERE aircraft_altitude <= ?
                            AND aircraft_id IN ( {aircraft_list} )
                            ORDER BY aircraft_id'''

select_flights_date = '''SELECT DISTINCT aircraft_id,Date(message_timestamp) 
                            FROM messages 
                            WHERE aircraft_altitude <= ?
                            AND DATE(message_timestamp) BETWEEN ?
                            AND ?
                            ORDER BY aircraft_id'''

select_flights_aircraft_date = '''SELECT DISTINCT aircraft_id,Date(message_timestamp) 
                            FROM messages 
                            WHERE aircraft_altitude <= ?
                            AND aircraft_id IN ( {aircraft_list} )
                            AND DATE(message_timestamp) BETWEEN ?
                            AND ?
                            ORDER BY aircraft_id'''

select_registration = "SELECT * FROM registrations WHERE aircraft_id = ?"


intersecting_trips = """SELECT trips.*, AsText(geom), Intersects((SELECT geom FROM polygons), geom) AS intersects 
                        FROM trips 
                        WHERE intersects = 1"""


select_messages = '''SELECT aircraft_lon, aircraft_lat, aircraft_altitude, message_timestamp
                            FROM messages WHERE aircraft_id = ?
                            AND aircraft_altitude BETWEEN ?
                            AND ?
                            AND Date(message_timestamp) = ?
                            ORDER BY message_timestamp'''

select_icao = '''SELECT aircraft_id FROM registrations WHERE tail_number = ?'''
